#!/bin/bash
#SBATCH --mem=32G
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --time=0:1:0    
#SBATCH --mail-user=<s.shahabodni33@gmail.com>
#SBATCH --mail-type=ALL

cd projects/def-arashmoh/jamalsm/vit-vit
module purge
module load python/3.10.2 scipy-stack
source ~/py37/bin/activate

python vit-vit/Testing.py