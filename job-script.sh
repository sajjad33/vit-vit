#SBATCH --nodes==1
#SBATCH --mem=4G
#SBATCH --time=00:10:00
#SBATCH --gres=gpu:v100:4
#SBATCH --ntasks-per-node=4
#SBATCH --mail-user=s.shahabodini33@gmail.com
#SBATCH --mail-type=ALL

python main.py
