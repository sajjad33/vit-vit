import random 
from tqdm import tqdm 
import wandb 
import time 
import os

wandb.init(project='icc-tutorial') 
           
os.environ['WANDB_API_KEY'] = 'e367b0d76e46baa9c909bb469a5e5c23ba474bac'
os.environ['WANDB_USERNAME'] = 'lanesar33' 

acc = [] 
for epoch in tqdm(range(1000)): 
    acc.append(random.randint(0,100)) 
    time.sleep(0.2) 
    if epoch % 50 == 0: 
        print("Epoch:", epoch, "Accuracy:", acc[epoch]) 
        wandb.log({
            "Epoch": epoch,
            "Accuracy": acc[epoch]
        })
